## CardiacIno Microcontroller Program (Arduino)

This branch maintains the Arduino project of CardiacIno Program for Microcontroller.
To make your own CardiacIno device, you'll need to get a NodeMCU WiFi board.
Download the Arduino IDE and install the libraries **ArduinoJson** and **ESP8266**.
Now, you'll need to open this project through the IDE and Build & Upload the program.
