#include <ArduinoJson.h>
#include <ESP8266WiFi.h>

// Wifi related definitions
#define SSID "CARDIACINO"
#define PASS "12345678"
IPAddress local_ip(192,168,4,1);
IPAddress gateway(192,168,4,1);
IPAddress subnet(255,255,255,0);
WiFiServer server(80);

// Sensor constants
#define PIN A0
#define LED D0
#define THRESHOLD 560
#define MAXIMA 512
#define MINIMA 512
// Vars
volatile int threshold = THRESHOLD;
volatile int value, BPM, IBI, ibi[10];
volatile unsigned long lastmillis = 0, currentmillis = 0, countermillis = 0,
    lastbeatmillis = 0;
volatile int timemillis;
volatile int P = MAXIMA, T = MINIMA, AMP = 0;
volatile boolean pulse = false, first = true, second = false;

void setup() {
  // Setup pins
  pinMode(PIN, INPUT);
  pinMode(LED, OUTPUT);
  Serial.begin(115200);
  delay(100);
  // Create wifi access point
  WiFi.mode(WIFI_AP);
  WiFi.softAP(SSID, PASS);
  WiFi.softAPConfig(local_ip, gateway, subnet);
  Serial.print("IP: ");
  Serial.println(WiFi.softAPIP());
  delay(100);
  // Setup wifi server
  server.begin();
  Serial.println("Server Running!");
  delay(200);
  // timestamp
  lastmillis=millis();
}

void loop() {
  WiFiClient client = server.available();
  if(!client) return;
  while(client.connected()){
    if(client.available()){
      calcpulse();
      DynamicJsonDocument json(1024);
      json["val"] = value;
      json["bpm"] = BPM;
      json["ibi"] = IBI;
      String text = "";
      serializeJson(json, text);
      client.println(text);
    }
  }
}

void calcpulse() {
  // Read the value from the sensor
  value = analogRead(PIN);
  // find the time diffrence and related values
  currentmillis = millis();
  int dt = currentmillis - lastmillis;
  lastmillis = currentmillis;
  countermillis += dt;
  timemillis += dt;
  int N = countermillis - lastbeatmillis;
  // Check for minima
  if(value < threshold && N > ((IBI/5)*3)) {
    if(value < T) {
      T = value;
    }
  }
  // check for maxima
  if(value > threshold && value > P) {
    P = value;
  }

  // Ready to check for heart beat
  if(N > 250) {
    if(value > threshold && !pulse && N > ((IBI/5)*3)) {
      pulse = true;
      digitalWrite(LED, LOW);
      // calculae Inter Beat Interval
      IBI = countermillis - lastbeatmillis;
      lastbeatmillis = countermillis;

      if(first) {
        first = false;
        second = true;
        return;
      }

      if(second) {
        second = false;
        for(int i=0; i<10; i++) {
          ibi[i] = IBI;
        }
      }
      // find the average for ten consecutive beats
      unsigned short int ibitotal = 0;
      for(int i=0; i<9; i++) {
        ibi[i] = ibi[i+1];
        ibitotal += ibi[i];
      }

      ibi[9] = IBI;
      ibitotal += ibi[9];
      ibitotal /= 10;
      // find the BPM from the average
      BPM = 60000/ibitotal;
    }
  }
  // the pulse is going down
  if(value < threshold && pulse) {
    digitalWrite(LED, HIGH);
    pulse = false;
    AMP = P - T;
    threshold = AMP/2 + T;
    P = threshold;
    T = threshold;
  }
  // Reset on inactivity
  if(N > 2500) {
    threshold = THRESHOLD;
    P = MAXIMA;
    T = MINIMA;
    lastbeatmillis = countermillis;
    first = true;
    second = false;
    BPM = 0;
  }
}
